
package com.app.onlinegift.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.app.onlinegift.constant.AppConstant;
import com.app.onlinegift.dto.OrderDto;
import com.app.onlinegift.dto.OrderDtoEdit;
import com.app.onlinegift.dto.OrderResponse;
import com.app.onlinegift.dto.ReciverDto;
import com.app.onlinegift.entity.Gift;
import com.app.onlinegift.entity.MyOrder;
import com.app.onlinegift.entity.Reciver;
import com.app.onlinegift.entity.User;
import com.app.onlinegift.repository.GiftRepository;
import com.app.onlinegift.repository.MyOrderRepository;
import com.app.onlinegift.repository.ReciverRepository;
import com.app.onlinegift.repository.UserRepository;
import com.app.onlinegift.serviceimpl.OrderServiceImpl;

@RunWith(SpringJUnit4ClassRunner.class)
public class OrderServiceImplTest {

	@InjectMocks
	OrderServiceImpl orderServiceImpl;

	@Mock
	UserRepository userRepository;

	@Mock
	MyOrderRepository myorderRepository;

	@Mock
	GiftRepository giftRepository;

	@Mock
	ReciverRepository reciverRepository;

	@Mock
	JavaMailSender javaMailSender;

	OrderResponse response = new OrderResponse();
	OrderDto orderDto = new OrderDto();
	List<ReciverDto> reciverlist = new ArrayList<>();
	List<Reciver> rlist = new ArrayList<>();
	OrderDtoEdit orderDtoEdit = new OrderDtoEdit();
	User user = new User();
	Gift gift = new Gift();
	MyOrder order = new MyOrder();
	Reciver recivers = new Reciver();
	SimpleMailMessage message = new SimpleMailMessage();

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		reciverlist = Stream.of(new ReciverDto("John", "john@gmail.com", "Happy"),
				new ReciverDto("Marc", "marc@gmail.com", "Happy"), new ReciverDto("Anu", "anu@gmail.com", "Welcome"))
				.collect(Collectors.toList());

		orderDto.setGiftId(1L);
		orderDto.setName("Tania");
		orderDto.setEmail("Tania@gmail.com");
		orderDto.setPhoneNo(976786786);
		orderDto.setQuantity(3);
		orderDto.setUserType(AppConstant.NORMAL);
		orderDto.setReciverlist(reciverlist);
		orderDto.setStatus(AppConstant.CONFIRMED);

		gift.setgId(1L);
		gift.setGiftId(12L);
		gift.setAvailableQuantity(10);
		gift.setGiftname("Frame");
		gift.setPrice(500.0);

		user.setUserId(17L);
		user.setEmail("tania@gmail.com");
		user.setName("Tania");
		user.setPhoneNo(86758768);

		rlist = Stream
				.of(new Reciver(1L, "John", "john@gmail.com", "Happy", "Confirmed", user, order),
						new Reciver(2L, "Marc", "marc@gmail.com", "Happy", "Confirmed", user, order),
						new Reciver(3L, "Anu", "anu@gmail.com", "Welcome", "Confirmed", user, order))
				.collect(Collectors.toList());

		order.setDate(LocalTime.now().toString());
		order.setOrderId(2L);
		order.setOrderNo(200L);
		order.setGift(gift);
		order.setUser(user);
		order.setQty(3);
		order.setStatus(orderDto.getStatus());
		order.setReciver(rlist);

		orderDtoEdit.setGiftId(gift.getGiftId());
		orderDtoEdit.setName("Tania");
		orderDtoEdit.setOrderNo(200);
		orderDtoEdit.setUserType(AppConstant.NORMAL);
		orderDtoEdit.setStatus(AppConstant.CONFIRMED);
		orderDtoEdit.setReciverlist(reciverlist);
	}

	@Test
	public void test_placeorder_success() {
		when(userRepository.save(user)).thenReturn((user));
		when(giftRepository.findById(Mockito.any())).thenReturn(Optional.of(gift));
		when(myorderRepository.save(order)).thenReturn(order);
		when(reciverRepository.save(recivers)).thenReturn(recivers);

		OrderResponse response = orderServiceImpl.placeorder(orderDto);
		assertEquals(AppConstant.EMAIL, response.getMessage());
		verify(giftRepository, times(1)).findById(1L);
	}

	@Test
	public void test_alterPlacedOrder_success() {
		when(myorderRepository.findByOrderNo(Mockito.anyLong())).thenReturn(Optional.of(order));
		when(userRepository.save(user)).thenReturn((user));
		when(giftRepository.findById(Mockito.any())).thenReturn(Optional.of(gift));
		when(myorderRepository.save(order)).thenReturn(order);
		when(reciverRepository.save(recivers)).thenReturn(recivers);

		OrderResponse response = orderServiceImpl.alterPlacedOrder(orderDtoEdit);
		assertEquals(AppConstant.ORDER_PLACEDS, response.getMessage());
		verify(giftRepository, times(1)).findById(Mockito.anyLong());
	}

}
