package com.app.onlinegift.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.app.onlinegift.dto.GiftDto;
import com.app.onlinegift.entity.Gift;
import com.app.onlinegift.repository.GiftRepository;
import com.app.onlinegift.serviceimpl.GiftServiceImpl;

@RunWith(SpringJUnit4ClassRunner.class)
public class GiftServiceImplTest {
	
	@InjectMocks
	GiftServiceImpl giftServiceImpl;

	@Mock
	GiftRepository giftRepository;
	
	List<GiftDto> giftList = new ArrayList<>();
	List<Gift> gList = new ArrayList<>();
		
	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		giftList = Stream.of(new GiftDto(12, "Frame", 120.0),
						     new GiftDto(15, "Teddy", 200.0))
				            .collect(Collectors.toList());
		
		gList = Stream.of(new Gift(12L,15L, "Frame", 120.0,10),
						  new Gift(15L,13L, "Teddy", 200.0, 15))
				          .collect(Collectors.toList());
	 }	
		
	@Test
	public void test_getAllGifts_success() {
		when(giftRepository.findAll()).thenReturn(gList);	
		List<GiftDto> expected = giftServiceImpl.getAllGifts();
		assertEquals(2, expected.size());
		verify(giftRepository, times(1)).findAll();
	}
}
	