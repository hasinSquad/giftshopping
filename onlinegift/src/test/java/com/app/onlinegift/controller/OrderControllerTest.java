package com.app.onlinegift.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.app.onlinegift.constant.AppConstant;
import com.app.onlinegift.dto.OrderDto;
import com.app.onlinegift.dto.OrderDtoEdit;
import com.app.onlinegift.dto.OrderResponse;
import com.app.onlinegift.dto.ReciverDto;
import com.app.onlinegift.entity.Gift;
import com.app.onlinegift.entity.User;
import com.app.onlinegift.service.OrderService;

@RunWith(SpringJUnit4ClassRunner.class)
public class OrderControllerTest {
	
	@InjectMocks
	OrderController orderController;

	@Mock
	OrderService orderService;
	
	OrderResponse response = new OrderResponse();
	OrderDto orderDto = new OrderDto();
	OrderDtoEdit editdto = new OrderDtoEdit();
	List<ReciverDto> reciverlist = new ArrayList<>();
	User user = new User();
	Gift gift = new Gift();
		
	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		reciverlist = Stream.of(new ReciverDto("John", "john@gmail.com", "Happy"),
				new ReciverDto("Marc", "marc@gmail.com", "Happy"), new ReciverDto("Anu", "anu@gmail.com", "Welcome"))
				.collect(Collectors.toList());
		orderDto.setGiftId(1L);
		orderDto.setName("Tania");
		orderDto.setEmail("Tania@gmail.com");
		orderDto.setPhoneNo(976786786);
		orderDto.setQuantity(3);
		orderDto.setUserType(AppConstant.NORMAL);
		orderDto.setReciverlist(reciverlist);
		orderDto.setStatus("temporary");	
		gift.setgId(1L);
		gift.setGiftId(12L);
		gift.setAvailableQuantity(10);
		gift.setGiftname("Frame");
		gift.setPrice(500.0);

		user.setUserId(17L);
		user.setEmail("tania@gmail.com");
		user.setName("Tania");
		user.setPhoneNo(86758768);
		
		editdto.setOrderNo(12L);
		editdto.setUserType("corporate");
		editdto.setName("John");
	 }	
		
	@Test
	public void test_placeorder_success() {
		when(orderService.placeorder(orderDto)).thenReturn(response);
		ResponseEntity<OrderResponse> expected =orderController.placeorder(orderDto);
		assertEquals(201, expected.getStatusCodeValue());
		assertNotNull(expected);
		verify(orderService, times(1)).placeorder(orderDto);
	}
	
	@Test
	public void test_alterPlacedOrder_success() {
		when(orderService.alterPlacedOrder(editdto)).thenReturn(response);
		ResponseEntity<OrderResponse> expected =orderController.alterPlacedOrder(editdto);
		assertEquals(201, expected.getStatusCodeValue());
		assertNotNull(expected);
		verify(orderService, times(1)).alterPlacedOrder(editdto);
	}
}
