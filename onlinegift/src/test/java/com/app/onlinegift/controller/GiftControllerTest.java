package com.app.onlinegift.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.app.onlinegift.dto.GiftDto;
import com.app.onlinegift.service.GiftService;

@RunWith(SpringJUnit4ClassRunner.class)
public class GiftControllerTest {	

	@InjectMocks
	GiftController giftController;

	@Mock
	GiftService giftService;
	
	List<GiftDto> giftList = new ArrayList<>();
		
	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		giftList = Stream.of(new GiftDto(12, "Frame", 120.0),
						     new GiftDto(15, "Teddy", 200.0))
				          .collect(Collectors.toList());
	}	
	
	@Test
	public void test_getAllGifts_success() {
		when(giftService.getAllGifts()).thenReturn(giftList);
		List<GiftDto> expected = giftService.getAllGifts();
		giftController.getAllGifts();
		assertEquals(2, expected.size());	
	}
	
}
