package com.app.onlinegift.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.onlinegift.dto.GiftDto;
import com.app.onlinegift.entity.Gift;
import com.app.onlinegift.repository.GiftRepository;
import com.app.onlinegift.service.GiftService;

@Service
public class GiftServiceImpl implements GiftService {

	@Autowired
	GiftRepository giftRepository;

	@Override
	public List<GiftDto> getAllGifts() {
		List<GiftDto> giftDtoList = new ArrayList<>();

		List<Gift> giftList = giftRepository.findAll();

		giftList.forEach(gift -> {
			GiftDto giftDto = new GiftDto();
			giftDto.setGiftId(gift.getGiftId());
			giftDto.setGiftName(gift.getGiftname());
			giftDto.setPrice(gift.getPrice());
			giftDtoList.add(giftDto);
		});
		return giftDtoList;
	}

}
