package com.app.onlinegift.serviceimpl;

import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.app.onlinegift.constant.AppConstant;
import com.app.onlinegift.dto.OrderDto;
import com.app.onlinegift.dto.OrderDtoEdit;
import com.app.onlinegift.dto.OrderResponse;
import com.app.onlinegift.entity.Gift;
import com.app.onlinegift.entity.MyOrder;
import com.app.onlinegift.entity.Reciver;
import com.app.onlinegift.entity.User;
import com.app.onlinegift.exception.OrderException;
import com.app.onlinegift.repository.GiftRepository;
import com.app.onlinegift.repository.MyOrderRepository;
import com.app.onlinegift.repository.ReciverRepository;
import com.app.onlinegift.repository.UserRepository;
import com.app.onlinegift.service.OrderService;

@Service
public class OrderServiceImpl implements OrderService {
	@Autowired
	UserRepository userRepository;

	@Autowired
	MyOrderRepository myorderRepository;

	@Autowired
	GiftRepository giftRepository;

	@Autowired
	ReciverRepository reciverRepository;

	@Autowired
	JavaMailSender javaMailSender;

	@Override
	public OrderResponse placeorder(OrderDto orderDto) {
		String userType = orderDto.getUserType();

		if ((userType.equalsIgnoreCase(AppConstant.CORPORATE) && orderDto.getReciverlist().size() > 4)
				|| userType.equalsIgnoreCase(AppConstant.NORMAL) && orderDto.getReciverlist().size() < 4) {
			User user = new User();
			user.setEmail(orderDto.getEmail());
			user.setName(orderDto.getName());
			user.setPhoneNo(orderDto.getPhoneNo());
			User users = userRepository.save(user);
			MyOrder order = new MyOrder();
			int orderNo = (int) (Math.random() * 10000) + 1;
			order.setOrderNo(orderNo);
			order.setDate(LocalTime.now().toString());
			Optional<Gift> gift = giftRepository.findById(orderDto.getGiftId());
			if (gift.isPresent())
				order.setGift(gift.get());
			order.setQty(orderDto.getQuantity());
			order.setStatus(orderDto.getStatus());
			order.setUser(users);
			order.setStatus(orderDto.getStatus());
			MyOrder orders = myorderRepository.save(order);
			orderDto.getReciverlist().forEach(reciver -> {
				Reciver recivers = new Reciver();
				recivers.setCustomizedMsg(reciver.getCustomizedMsg());
				recivers.setEmail(reciver.getEmail());
				recivers.setReceiverName(reciver.getReceiverName());
				recivers.setOrder(orders);
				recivers.setUser(users);
				reciverRepository.save(recivers);
			});
			if (orderDto.getStatus().equalsIgnoreCase(AppConstant.CONFIRMED)) {
				orderDto.getReciverlist().forEach(reciver -> {
					SimpleMailMessage message = new SimpleMailMessage();
					message.setTo(reciver.getEmail());
					message.setBcc("hasin22m@gmail.com");
					message.setSubject("Online Gift SpringBootApplication Demo");
					message.setText(reciver.getCustomizedMsg());

					javaMailSender.send(message);
				});
				return new OrderResponse(AppConstant.ORDER_PLACED, AppConstant.EMAIL, orderNo);
			}
			return new OrderResponse(AppConstant.ORDER_PLACED, AppConstant.ORDER_PENDING, orderNo);
		} else {

			throw new OrderException(AppConstant.ORDER_NOT_SUCCESS);
		}
	}

	@Override
	public OrderResponse alterPlacedOrder(OrderDtoEdit orderDtoEdit) {
		Optional<MyOrder> orders = myorderRepository.findByOrderNo(orderDtoEdit.getOrderNo());
		if (orders.isPresent()) {
			List<Reciver> reciver3 = orders.get().getReciver();
			for (Reciver r : reciver3) {
				reciverRepository.deleteById(r.getRid());
			}
			String userType = orderDtoEdit.getUserType();
			if ((userType.equalsIgnoreCase(AppConstant.CORPORATE) && orderDtoEdit.getReciverlist().size() > 4)
					|| userType.equalsIgnoreCase(AppConstant.NORMAL) && orderDtoEdit.getReciverlist().size() < 4) {
				User user = orders.get().getUser();
				user.setEmail(orderDtoEdit.getEmail());
				user.setName(orderDtoEdit.getName());
				user.setPhoneNo(orderDtoEdit.getPhoneNo());
				User userUpdate = userRepository.save(user);
				MyOrder order = new MyOrder();
				order.setDate(LocalTime.now().toString());
				Optional<Gift> gift = giftRepository.findById(orderDtoEdit.getGiftId());
				if (gift.isPresent())
				order.setGift(gift.get());
				order.setOrderNo(user.getUserId() * 100);
				order.setQty(orderDtoEdit.getQuantity());
				order.setStatus(orderDtoEdit.getStatus());
				order.setUser(user);
				order.setStatus(orderDtoEdit.getStatus());
				order.setOrderId(orders.get().getOrderId());
				myorderRepository.save(order);

				orderDtoEdit.getReciverlist().forEach(reciver -> {
					Reciver recivers = new Reciver();
					recivers.setCustomizedMsg(reciver.getCustomizedMsg());
					recivers.setEmail(reciver.getEmail());
					recivers.setReceiverName(reciver.getReceiverName());
					recivers.setOrder(orders.get());
					recivers.setUser(userUpdate);
					reciverRepository.save(recivers);
				});

				if (orderDtoEdit.getStatus().equalsIgnoreCase(AppConstant.CONFIRMED)) {
					orderDtoEdit.getReciverlist().forEach(reciver -> {
						SimpleMailMessage message = new SimpleMailMessage();
						message.setTo(reciver.getEmail());
						message.setBcc("hasin22m@gmail.com");
						message.setSubject("Online Gift SpringBootApplication Demo");
						message.setText(reciver.getCustomizedMsg());
						javaMailSender.send(message);
					});
				}
				return new OrderResponse(AppConstant.ORDER_PLACED, AppConstant.ORDER_PLACEDS,
						orders.get().getOrderNo());
			} else {
				throw new OrderException(AppConstant.ORDER_NOT_SUCCESS);
			}
		} else {
			throw new OrderException("no order palced");
		}
	}

}
