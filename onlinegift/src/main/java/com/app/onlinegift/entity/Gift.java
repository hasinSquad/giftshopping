package com.app.onlinegift.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Gift {
@Id        
@GeneratedValue(strategy = GenerationType.IDENTITY)       
    private long gId;
    private long giftId;
    private String giftname;
    private double price;
    private int availableQuantity;
    
    public Gift() {
    	
    }
    
	public Gift(long gId, long giftId, String giftname, double price, int availableQuantity) {
		super();
		this.gId = gId;
		this.giftId = giftId;
		this.giftname = giftname;
		this.price = price;
		this.availableQuantity = availableQuantity;
	}
	public long getgId() {
		return gId;
	}
	public void setgId(long gId) {
		this.gId = gId;
	}
	public long getGiftId() {
		return giftId;
	}
	public void setGiftId(long giftId) {
		this.giftId = giftId;
	}
	public String getGiftname() {
		return giftname;
	}
	public void setGiftname(String giftname) {
		this.giftname = giftname;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public int getAvailableQuantity() {
		return availableQuantity;
	}
	public void setAvailableQuantity(int availableQuantity) {
		this.availableQuantity = availableQuantity;
	}
	
    

}
