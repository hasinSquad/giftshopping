package com.app.onlinegift.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Reciver {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long rid;

	String receiverName;
	String email;
	String customizedMsg;
	String status;

	@ManyToOne
	@JoinColumn
	User user;

	@JoinColumn
	@ManyToOne
	MyOrder order;
	
	public Reciver() {
		
	}

	public Reciver(Long rid, String receiverName, String email, String customizedMsg, String status, User user,
			MyOrder order) {
		super();
		this.rid = rid;
		this.receiverName = receiverName;
		this.email = email;
		this.customizedMsg = customizedMsg;
		this.status = status;
		this.user = user;
		this.order = order;
	}

	public Long getRid() {
		return rid;
	}

	public void setRid(Long rid) {
		this.rid = rid;
	}

	public String getReceiverName() {
		return receiverName;
	}

	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCustomizedMsg() {
		return customizedMsg;
	}

	public void setCustomizedMsg(String customizedMsg) {
		this.customizedMsg = customizedMsg;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public MyOrder getOrder() {
		return order;
	}

	public void setOrder(MyOrder order) {
		this.order = order;
	}

}
