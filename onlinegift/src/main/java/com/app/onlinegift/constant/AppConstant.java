package com.app.onlinegift.constant;

public class AppConstant {

	public static final String ORDER_PLACEDS = "YOUR ORDER IS Successfull, Please check your mail!";
	public static final String ORDER_NOT_SUCCESS = "YOUR ORDER IS NOT Successfully!";
	public static final String ORDER_PENDING = "YOUR ORDER IS Successfull, Please confirm to proceed!";
	public static final String EMAIL = "EMAIL SENT PLS CHECK";
	public static final int ORDER_PLACED = 600;
	public static final int ORDER_NOT_PLACED = 610;
	public static final String CONFIRMED = "Confirmed";
	public static final String CORPORATE = "Corporate";
	public static final String NORMAL = "normal";
	
	private AppConstant() {
		
	}

}
