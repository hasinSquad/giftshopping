package com.app.onlinegift;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnlinegiftApplication {

	public static void main(String[] args) {
		SpringApplication.run(OnlinegiftApplication.class, args);
	}

}
