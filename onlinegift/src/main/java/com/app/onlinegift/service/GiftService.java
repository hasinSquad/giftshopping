package com.app.onlinegift.service;

import java.util.List;

import com.app.onlinegift.dto.GiftDto;

public interface GiftService {
	  public List<GiftDto> getAllGifts();

}
