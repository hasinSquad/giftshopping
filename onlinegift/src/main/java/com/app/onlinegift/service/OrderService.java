package com.app.onlinegift.service;

import com.app.onlinegift.dto.OrderDto;
import com.app.onlinegift.dto.OrderDtoEdit;
import com.app.onlinegift.dto.OrderResponse;

public interface OrderService {
OrderResponse	placeorder(OrderDto orderDto);
OrderResponse alterPlacedOrder(OrderDtoEdit orderDtoEdit);
}
