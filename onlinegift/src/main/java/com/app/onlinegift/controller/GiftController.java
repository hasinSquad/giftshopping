package com.app.onlinegift.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.onlinegift.dto.GiftDto;
import com.app.onlinegift.service.GiftService;

@RestController
@RequestMapping("/gifts")

public class GiftController {

	@Autowired
	GiftService giftService;

	/**
	 * @author Hasina
	 * @param displays list of gifts available to order by the user
	 * @return the Success message with corresponding status code
	 */
	@GetMapping
	public ResponseEntity<List<GiftDto>> getAllGifts() {
		return new ResponseEntity<>(giftService.getAllGifts(), HttpStatus.OK);

	}
}