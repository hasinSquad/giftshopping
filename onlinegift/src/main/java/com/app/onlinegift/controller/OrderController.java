package com.app.onlinegift.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.onlinegift.dto.OrderDto;
import com.app.onlinegift.dto.OrderDtoEdit;
import com.app.onlinegift.dto.OrderResponse;
import com.app.onlinegift.service.OrderService;

@RestController
@RequestMapping("/orders")
public class OrderController {
	
	@Autowired
	OrderService orderService;

	/**
	 * @author Rakesh
	 * @param order placed by the user based on the usertype
	 * @return the Success message with corresponding status code
	 */
	@PostMapping
	public ResponseEntity<OrderResponse> placeorder(@RequestBody OrderDto orderDto) {
		return new ResponseEntity<>(orderService.placeorder(orderDto),HttpStatus.CREATED);

	}
	
	/**
	 * @author Rakesh
	 * @param update the order placed by the user based on the usertype 
	 * @return the Success message with corresponding status code
	 */
	@PutMapping
	public ResponseEntity<OrderResponse> alterPlacedOrder(@RequestBody OrderDtoEdit orderDtoEdit) {
		return new ResponseEntity<>(orderService.alterPlacedOrder(orderDtoEdit),HttpStatus.CREATED);
	}

}
