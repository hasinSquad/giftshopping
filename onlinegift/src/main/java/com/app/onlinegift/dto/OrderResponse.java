package com.app.onlinegift.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderResponse {
	 int statusCode;
	 String message;
	 long orderId;
	/**
	 * @param statusCode
	 * @param message
	 * @param orderId
	 */
	public OrderResponse(int statusCode, String message, long orderId) {
		this.statusCode = statusCode;
		this.message = message;
		this.orderId = orderId;
	}
	/**
	 * 
	 */
	public OrderResponse() {
		super();
		
	}
	public int getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public long getOrderId() {
		return orderId;
	}
	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}
	
	
}
