package com.app.onlinegift.dto;

import java.util.List;

public class OrderDto {

	long giftId;
	String name;
	String email;
	long phoneNo;
	String userType;
	int quantity;
	String status;

	List<ReciverDto> reciverlist;

	public long getGiftId() {
		return giftId;
	}

	public void setGiftId(long giftId) {
		this.giftId = giftId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public long getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(long phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<ReciverDto> getReciverlist() {
		return reciverlist;
	}

	public void setReciverlist(List<ReciverDto> reciverlist) {
		this.reciverlist = reciverlist;
	}
	
	

}
