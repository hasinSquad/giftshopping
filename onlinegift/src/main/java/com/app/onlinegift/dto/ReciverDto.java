package com.app.onlinegift.dto;

public class ReciverDto {
	
	String receiverName;
	String email;
	String customizedMsg;
	
	public ReciverDto() {
		
	}
	
	public ReciverDto(String receiverName, String email, String customizedMsg) {
		super();
		this.receiverName = receiverName;
		this.email = email;
		this.customizedMsg = customizedMsg;
	}
	public String getReceiverName() {
		return receiverName;
	}
	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCustomizedMsg() {
		return customizedMsg;
	}
	public void setCustomizedMsg(String customizedMsg) {
		this.customizedMsg = customizedMsg;
	}	
	
}
