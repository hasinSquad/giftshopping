package com.app.onlinegift.dto;

public class GiftDto {

	 private long giftId;
	    private String giftName;
	    private double price;
	
	   public GiftDto() {
		   
	   }
	    public GiftDto(long giftId, String giftName, double price) {
			super();
			this.giftId = giftId;
			this.giftName = giftName;
			this.price = price;
		}
		public long getGiftId() {
	        return giftId;
	    }
	    public void setGiftId(long giftId) {
	        this.giftId = giftId;
	    }
	    public String getGiftName() {
	        return giftName;
	    }
	    public void setGiftName(String giftName) {
	        this.giftName = giftName;
	    }
	    public double getPrice() {
	        return price;
	    }
	    public void setPrice(double price) {
	        this.price = price;
	    }
	   
}
