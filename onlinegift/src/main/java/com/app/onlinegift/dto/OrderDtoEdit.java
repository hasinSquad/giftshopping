package com.app.onlinegift.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderDtoEdit extends OrderDto {
	long orderNo;

	public long getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(long orderNo) {
		this.orderNo = orderNo;
	}
	
	
}
