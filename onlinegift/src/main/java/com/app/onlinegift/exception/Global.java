package com.app.onlinegift.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.app.onlinegift.constant.AppConstant;


@ControllerAdvice
public class Global extends ResponseEntityExceptionHandler {
	
  
	@ExceptionHandler(OrderException.class)
	ResponseEntity<ErrorStatus> showresponse(OrderException custumException){
		ErrorStatus er= new ErrorStatus();
		er.setStatuscode(AppConstant.ORDER_NOT_PLACED);
		er.setStatusmessage(custumException.getMessage());
		return new ResponseEntity<>(er,HttpStatus.OK);
		
	}
}