package com.app.onlinegift.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import com.app.onlinegift.entity.Reciver;
@Repository
public interface ReciverRepository extends JpaRepository<Reciver, Long> {

}
