package com.app.onlinegift.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.onlinegift.entity.MyOrder;

@Repository
public interface MyOrderRepository extends JpaRepository<MyOrder, Long> {
    Optional<MyOrder> findByOrderNo(long orderNo);
}
