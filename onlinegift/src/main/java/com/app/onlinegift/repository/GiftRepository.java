package com.app.onlinegift.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.onlinegift.entity.Gift;
@Repository

public interface GiftRepository extends JpaRepository<Gift, Long> {

}
